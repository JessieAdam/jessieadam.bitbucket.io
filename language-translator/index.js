const inputForm = document.getElementById("english-trans");
const output = document.getElementById("output");

inputForm.addEventListener("submit", (e) => {
  e.preventDefault();

  output.innerHTML = translate(
    e.target.english.value,
    e.target.vowels_offset.value,
    e.target.consonants_offset.value
  );
});

const consonants = "bcdfghjklmnpqrstvwxyz";
const vowels = "aeiou";

const translate = (text, vowelsOffset, consonantsOffset) => {
  text = text.toLowerCase();
  vowelsOffset = parseInt(vowelsOffset);
  consonantsOffset = parseInt(consonantsOffset);

  let output = "";

  for (let i = 0; i < text.length; i++) {
    if (consonants.includes(text[i])) {
      let letIndex = consonants.indexOf(text[i]);

      letIndex += consonantsOffset;
      if (letIndex >= consonants.length) {
        letIndex -= consonants.length;
      } else if (letIndex < 0) {
        letIndex += consonants.length;
      }

      output += consonants[letIndex];
    } else if (vowels.includes(text[i])) {
      let letIndex = vowels.indexOf(text[i]);

      letIndex += vowelsOffset;
      if (letIndex >= vowels.length) {
        letIndex -= vowels.length;
      } else if (letIndex < 0) {
        letIndex += vowels.length;
      }

      output += vowels[letIndex];
    } else {
      output += text[i];
    }
  }

  return output;
};
