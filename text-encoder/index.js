const encodeInputElm = document.getElementById("encode-text-input");
const decodeInputElm = document.getElementById("decode-text-input");
const outputElm = document.getElementById("text-output");

encodeInputElm.addEventListener("submit", (e) => {
  e.preventDefault();

  encodeData(e.target.data.value);
});

decodeInputElm.addEventListener("submit", (e) => {
  e.preventDefault();

  decodeData(e.target.data.value);
});

const alphabette =
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!?,. \"'";
const randNum = Math.floor(Math.random() * 24 + 1);

const encodeData = (data) => {
  let output = "";

  for (let i = 0; i < data.length; i++) {
    let alphIndex = alphabette.indexOf(data[i]);

    if (alphIndex === -1) {
      output += data[i];
    } else {
      alphIndex += randNum;

      if (alphIndex > alphabette.length - 1) {
        alphIndex -= alphabette.length;
      }

      output += alphabette[alphIndex];
    }
  }

  outputElm.innerHTML = output;
};

const decodeData = (data) => {
  let output = "";

  for (let i = 0; i < data.length; i++) {
    let alphIndex = alphabette.indexOf(data[i]);

    if (alphIndex === -1) {
      output += data[i];
    } else {
      alphIndex -= randNum;

      if (alphIndex < 0) {
        alphIndex += alphabette.length;
      }

      output += alphabette[alphIndex];
    }
  }

  outputElm.innerHTML = output;
};
