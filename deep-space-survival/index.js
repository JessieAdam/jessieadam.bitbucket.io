const idleLogs = {
  start: [
    "The cool, unbreathed oxygen flows over your face as",
    "Floating in the very thin air,",
    "While searching through your old photos,",
    "While lingering on the past,",
    "Writing pointless logs for the sake your own sanity,",
    "You imagine if there might be ghosts in space as",
    '"Will I ever get there?", You think as',
    '"How have I gotten so dirty?", You think as',
  ],
  end: [
    "you wonder if you might have needed a friend",
    "you stare out into the empty abyss",
    "you feel just a bit more lonely",
    "a sudden feeling of fear flows over your weightless body",
    "the closest star seems to feel just a little warmer",
    "a deep, underlying sadness grows into an even deeper regret",
    "you begin to feel empty, yet you just ate",
    "you start to remember things, and feel just a little bit better",
  ],
};

const possableIssues = [
  {
    desc: "Leak in fuel tank",
    button: "repair fuel tank",
    danger: "fuel",
    energy: 5,
    removes: 3,
  },
  {
    desc: "Leak in oxygen tank",
    button: "repair oxygen tank",
    danger: "oxygen",
    energy: 5,
    removes: 3,
  },
  {
    desc: "Hole punctured in hull",
    button: "repare hull",
    danger: "health",
    energy: 15,
    removes: 2,
  },
  {
    desc: "Garden needs watering",
    button: "water garden",
    danger: "oxygen",
    energy: 1,
    removes: 1,
  },
  {
    desc: "Meteor shower",
    button: "maneuver from meteor shower",
    danger: "health",
    energy: 10,
    removes: 5,
  },
  {
    desc: "Water leak",
    button: "fix water pipes",
    danger: "water",
    energy: 5,
    removes: 3,
  },
  {
    desc: "Light bulb needs replacing",
    button: "replace light bulb",
    danger: "health",
    energy: 2,
    removes: 1,
  },
  {
    desc: "Your solid waste needs to be emptied",
    button: "empty solid waste",
    danger: "water",
    energy: 1,
    removes: 1,
  },
  {
    desc: "Ship needs cleaning",
    button: "clean ship",
    danger: "health",
    energy: 15,
    removes: 1,
  },
  {
    desc: "Ship off course",
    button: "redirect ship",
    danger: "health",
    energy: 10,
    removes: 5,
  },
];

const storyTextElement = document.getElementById("story-text");
const statusElms = {
  shipStatus: document.getElementById("ship-status"),
  healthButtons: document.getElementById("health-buttons"),
  bookStatus: document.getElementById("book-status"),
};

const storyHistory = [];
const shipStatus = {
  issues: [],
  visibleIssues: [],
  health: 100,
  water: 100,
  fuel: 100,
  oxygen: 100,
  energy: 100,
  food: 100,
  sanity: 100,
  working: false,
};

const storyLog = (text) => {
  storyHistory.unshift(text);

  storyTextElement.innerHTML = storyHistory
    .map((text) => `<p>${text}</p>`)
    .join("");
};

const makeIdleText = () => {
  const text = `
      <span class="yellow-text">
        ${idleLogs.start[Math.floor(Math.random() * idleLogs.start.length)]} ${
    idleLogs.end[Math.floor(Math.random() * idleLogs.end.length)]
  }
      </span>
    `;

  storyLog(text);

  shipStatus.sanity -= 1;

  reloadVisuals();
};

const checkSystems = () => {
  if (!shipStatus.working && shipStatus.energy >= 5) {
    shipStatus.working = true;
    shipStatus.energy -= 3;

    reloadVisuals();

    storyLog(`You check your ships working systems...`);

    setTimeout(() => {
      if (!shipStatus.issues.length) {
        storyLog(`Ship status: <span class="green-text">Ok</span>`);
      } else {
        statusElms.healthButtons.innerHTML = shipStatus.issues
          .map(
            (issue) => `
          <div class="row">
            <div class="col s12">
              <button class="btn grey darken-2" onclick='fixIssue(${JSON.stringify(
                issue
              )})'>${issue.button}</button>
            </div>
          </div>
        `
          )
          .join("");

        storyLog(`
          Ship status: <span class="red-text">
            Not ok.
            ${shipStatus.issues
              .map((shipIssue) => `<br />${shipIssue.desc}`)
              .join("")}
          </span>
        `);

        shipStatus.visibleIssues = [...shipStatus.issues];
      }

      shipStatus.working = false;
    }, 2000);
  }
};

const addIssue = () => {
  const possibleIssue =
    possableIssues[Math.floor(Math.random() * possableIssues.length)];

  if (!shipStatus.issues.includes(possibleIssue)) {
    shipStatus.issues.push(possibleIssue);
  }
};

const fixIssue = (issue) => {
  if (!shipStatus.working && shipStatus.energy >= 10) {
    shipStatus.energy -= issue.energy;

    reloadVisuals();

    const issueIndex = shipStatus.issues.findIndex(
      (shipIssue) => shipIssue.button === issue.button
    );
    const visibleIssueIndex = shipStatus.visibleIssues.findIndex(
      (shipIssue) => shipIssue.button === issue.button
    );

    shipStatus.issues.splice(issueIndex, 1);
    shipStatus.visibleIssues.splice(visibleIssueIndex, 1);

    statusElms.healthButtons.innerHTML = shipStatus.visibleIssues
      .map(
        (issue) =>
          `
            <div class="row">
              <div class="col s12">
                <button
                  class="btn grey darken-2"
                  onclick='fixIssue(${JSON.stringify(issue)})'
                >${issue.button}</button>
              </div>
            </div>
          `
      )
      .join("");
  }
};

const updateState = () => {
  shipStatus.energy += 1;
  if (shipStatus.energy > 100) {
    shipStatus.energy = 100;
  }

  shipStatus.issues.forEach((issue, i) => {
    shipStatus.sanity -= 1;
    shipStatus[issue.danger] -= issue.removes;
  });

  if (
    shipStatus.health > 0 &&
    shipStatus.fuel > 0 &&
    shipStatus.oxygen > 0 &&
    shipStatus.water > 0 &&
    shipStatus.food > 0 &&
    shipStatus.sanity > 0
  ) {
    reloadVisuals();
  } else {
    document.getElementById("game-screen").innerHTML = `
      <div class="col s12">
        <h1 class="center red-text">Game Over</h1>
      </div>
    `;

    clearInterval(gameInterval);
  }
};

const eatFood = () => {
  if (shipStatus.energy < 100) {
    shipStatus.energy += 5;
    shipStatus.food -= 1;
    if (shipStatus.energy > 100) {
      shipStatus.energy = 100;
    }

    storyLog(
      "You feel a surge of energy as the soft paste passes down your throat"
    );
    reloadVisuals();
  }
};

const reloadVisuals = () => {
  statusElms.shipStatus.innerHTML = `
    <h6>Health: ${shipStatus.health}%</h6>
    <h6>Fuel: ${shipStatus.fuel}%</h6>
    <h6>Oxygen: ${shipStatus.oxygen}%</h6>
    <h6>Water: ${shipStatus.water}%</h6>
    <h5>Energy: ${shipStatus.energy}%</h5>
    <h5>Food: ${shipStatus.food}%</h5>
    <h5>Sanity: ${shipStatus.sanity}%</h5>
  `;
};

// Read book function

const bookPages = [
  "What would it be like if you went into deep space for the rest of your life?",
  "Lets say you did this on your own. (Not very smart)",
  "But if you did, it would be very lonely.",
  "You would have to keep maintinence on your ship first of all.",
  "And you would have to keep yourself from going insane. (Such as reading by this book)",
  "Of course this is all just theoreticle.",
  "It is very unlikely that a person would be that stupid, and go out on his own.",
  "Especially if he knew that he would be there for the rest of his life.",
  "A person obviously has better things to do.",
  "Then again, maybe some people don't know the detication it takes to do something like that.",
  "But if you were to do this, you would probably see some crazy things.",
  "Such as some of the planets of the solar system.",
  "Or the amazing view of stars way off in the distance.",
  "But a lot of unexpected things can happen!",
  "Like fuel tank leaks.",
  "Or punctures in the hull of your ship, caused by small pieces of space junk.",
  "But there's a lot more than that!",
  "Water leaks.",
  "Oxygen tank leaks.",
  "Dying plants. (Used specifically for food and oxygen)",
  "Or, as said before, litteraly going insane!",
  "But aside from all that, for some people, it can be a very peaceful experience.",
  "And perhaps a nicer way than others to end your life with.",
  "This is a totaly fictional book meant to spark the imagination of people.",
  "The situation portraied in this book will most likely never happen on purpose.",
  "Story by -Yeshuae Kelly-",
  "Idea probably by -Yeshuae Kelly-",
];

let storyIndex = 0;

const readBook = () => {
  if (storyIndex < bookPages.length) {
    statusElms.bookStatus.innerHTML += `
      <div class="book-part">
        <span class="blue-text">${bookPages[storyIndex]}</span>
      </div>
    `;

    statusElms.bookStatus.scrollTop = statusElms.bookStatus.scrollHeight;

    shipStatus.sanity += 5;
    if (shipStatus.sanity > 100) {
      shipStatus.sanity = 100;
    }

    storyIndex++;
  } else {
    storyLog(
      "You have already finnished the book, and though you are saddened by it, you feel just a bit accomplished!"
    );

    shipStatus.sanity -= 1;
  }

  reloadVisuals();
};

const gameInterval = setInterval(() => {
  if (Math.random() < 0.15 && !shipStatus.working) {
    makeIdleText();
  }

  if (Math.random() < 0.15) {
    addIssue();
  }

  updateState();
}, 5000);

updateState();
