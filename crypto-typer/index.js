const mapElm = document.getElementById("map");

const map = [];
for (let i = 0; i < 12; i++) {
  const row = [];
  for (let i = 0; i < 12; i++) {
    const col = {};
    row.push(col);
  }
  map.push(row);
}

const drawToMap = () => {
  const mapHTML = map
    .map(
      (row) =>
        `<div class="row black" style="height: 8.33%; margin: 0;">${row
          .map(
            (col, i) =>
              `
                <div
                  class="col s1"
                  style="height: 100%; border: 1px solid white; padding: 0;"
                >${i}</div>
              `
          )
          .join("")}</div>`
    )
    .join("");

  mapElm.innerHTML = mapHTML;
};

drawToMap();
