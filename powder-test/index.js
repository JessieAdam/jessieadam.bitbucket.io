const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

class Pixel {
  constructor(x, y, type) {
    this.x = x;
    this.y = y;
    this.vx = 0;
    this.vy = 0;

    this.type = type;
    switch (type) {
      case "sand":
        this.mass = 2;
        break;
      default:
        this.mass = 1;
    }

    this.id = new Symbol();
  }

  doPhysics(pixels) {
    this.vy += this.mass / 4;
    this.y += this.vy;

    if (this.y + this.mass > canvas.height) {
      this.y = canvas.height - this.mass;
    }

    pixels.forEach((pixel, i) => {
      if (pixel.id !== this.id) {
        if (Math.abs(this.x - pixel.x) > ()) {

        }
      }
    });

  }

  draw() {
    let color;

    switch (this.type) {
      case "sand":
        color = "#eddb72";
        break;
      default:
    }

    ctx.fillStyle = color;

    ctx.beginPath();
    ctx.arc(this.x, this.y, this.mass, 0, Math.PI * 2);
    ctx.fill();
  }
}

let simulationStarted = false;
let gameInterval;

const startSimulation = () => {
  simulationStarted = true;

  if (gameInterval) {
    clearInterval(gameInterval);
  }

  const pixels = [];
  for (let i = 0; i < 20; i++) {
    pixels.push(
      new Pixel(
        Math.random() * canvas.width,
        Math.random() * canvas.height,
        "sand"
      )
    );
  }

  const draw = () => {
    if (clicking && pixels.length < 10000) {
      pixels.push(new Pixel(mousePos.x, mousePos.y, "sand"));
    }

    pixels.forEach((pixel, i) => {
      pixel.doPhysics();
      pixel.draw();
    });
  };

  ctx.font = "24px serif";

  let lastLoop = Date.now();
  let nextFpsCheck = 0;
  let fps = 60;
  let currentFps = 60;

  gameInterval = setInterval(() => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    draw();

    const thisLoop = Date.now();
    currentFps = Math.floor(1000 / (thisLoop - lastLoop));
    lastLoop = thisLoop;
    ctx.fillStyle = "white";
    ctx.fillText(`${fps} fps`, 10, 30);
    if (nextFpsCheck <= 0) {
      fps = currentFps;

      nextFpsCheck = 60;
    } else {
      nextFpsCheck--;
    }

    ctx.fillText(`Particles: ${pixels.length}`, 10, 60);
  }, 1000 / 60);
};

startSimulation();

let clicking = false;
let mousePos = { x: 0, y: 0 };

window.addEventListener("mousedown", () => {
  clicking = true;
});

canvas.addEventListener("mousemove", (e) => {
  mousePos = { x: e.offsetX, y: e.offsetY };
});

window.addEventListener("mouseup", () => {
  clicking = false;
});
