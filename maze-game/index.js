const saveText = (text, filename) => {
  const a = document.createElement("a");
  a.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," + encodeURIComponent(text)
  );
  a.setAttribute("download", filename);
  a.click();
};

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

const startGame = () => {
  document.body.removeChild(document.getElementById("start-button"));

  const maze = [];
  for (let i = 0; i < 30; i++) {
    const row = [];
    for (let j = 0; j < 30; j++) {
      row.push({
        left: true,
        right: true,
        top: true,
        down: true,
      });
    }
    maze.push(row);
  }

  const genMaze = () => {
    const point = {
      x: Math.floor(Math.random() * maze[0].length),
      y: Math.floor(Math.random() * maze.length),
    };

    let loopCount = 0;
    while (loopCount < 100) {
      const randNum = Math.floor(Math.random() * 4);

      switch (randNum) {
        case 0:
          maze[point.y][point.x].right = false;
          point.x += 1;
          break;
        case 1:
          maze[point.y][point.x].left = false;
          point.x -= 1;
          break;
        case 2:
          maze[point.y][point.x].down = false;
          point.y += 1;
          break;
        case 3:
          maze[point.y][point.x].top = false;
          point.y -= 1;
          break;
        default:
      }

      let hitEdge = false;
      if (point.x < 0) {
        point.x = 0;
        hitEdge = true;
      } else if (point.x > maze[0].length - 1) {
        point.x = maze[0].length - 1;
        hitEdge = true;
      }
      if (point.y < 0) {
        point.y = 0;
        hitEdge = true;
      } else if (point.y > maze.length - 1) {
        point.y = maze.length - 1;
        hitEdge = true;
      }

      if (!hitEdge) {
        switch (randNum) {
          case 0:
            maze[point.y][point.x].left = false;
            break;
          case 1:
            maze[point.y][point.x].right = false;
            break;
          case 2:
            maze[point.y][point.x].top = false;
            break;
          case 3:
            maze[point.y][point.x].down = false;
            break;
          default:
        }
      }

      loopCount++;
    }
  };

  let size;

  const drawMaze = () => {
    ctx.lineWidth = 4;

    maze.forEach((row, i) => {
      row.forEach((col, j) => {
        const pos = {
          x: j * size.width,
          y: i * size.height,
        };

        const dist = Math.hypot(j - player.x, i - player.y);

        if (dist < 8) {
          const color = {
            dark: 120 - dist * 25,
            light: 171 - dist * 25,
          };

          if (col.left && col.right && col.top && col.down) {
            ctx.fillStyle = `rgb(${color.light},${color.light},${color.light})`;
            ctx.fillRect(pos.x, pos.y, size.width, size.height);
          } else {
            ctx.fillStyle = `rgb(${color.dark},${color.dark},${color.dark})`;
            ctx.strokeStyle = `rgb(${color.light},${color.light},${color.light})`;

            ctx.fillRect(pos.x, pos.y, size.width, size.height);

            if (col.left) {
              ctx.beginPath();
              ctx.moveTo(pos.x, pos.y);
              ctx.lineTo(pos.x, pos.y + size.height);
              ctx.stroke();
            }

            if (col.right) {
              ctx.beginPath();
              ctx.moveTo(pos.x + size.width, pos.y);
              ctx.lineTo(pos.x + size.width, pos.y + size.height);
              ctx.stroke();
            }

            if (col.top) {
              ctx.beginPath();
              ctx.moveTo(pos.x, pos.y);
              ctx.lineTo(pos.x + size.width, pos.y);
              ctx.stroke();
            }

            if (col.down) {
              ctx.beginPath();
              ctx.moveTo(pos.x, pos.y + size.height);
              ctx.lineTo(pos.x + size.width, pos.y + size.height);
              ctx.stroke();
            }
          }
        }
      });
    });
  };

  const draw = () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    size = {
      width: canvas.height / maze[0].length,
      height: canvas.height / maze.length,
    };

    drawMaze();

    const charPos = {
      x: player.x * size.width + size.width / 2,
      y: player.y * size.height + size.height / 2,
    };

    ctx.fillStyle = "#b3a06f";

    switch (player.dir) {
      case "down":
        ctx.beginPath();
        ctx.arc(
          charPos.x + size.width / 2.5,
          charPos.y + size.height / 6,
          size.width / 7,
          0,
          Math.PI * 2
        );
        ctx.fill();

        ctx.beginPath();
        ctx.arc(
          charPos.x - size.width / 2.5,
          charPos.y + size.height / 6,
          size.width / 7,
          0,
          Math.PI * 2
        );
        ctx.fill();
        break;
      case "up":
        ctx.beginPath();
        ctx.arc(
          charPos.x + size.width / 2.5,
          charPos.y - size.height / 6,
          size.width / 7,
          0,
          Math.PI * 2
        );
        ctx.fill();

        ctx.beginPath();
        ctx.arc(
          charPos.x - size.width / 2.5,
          charPos.y - size.height / 6,
          size.width / 7,
          0,
          Math.PI * 2
        );
        ctx.fill();
        break;
      case "right":
        ctx.beginPath();
        ctx.arc(
          charPos.x + size.width / 6,
          charPos.y + size.height / 2.5,
          size.width / 7,
          0,
          Math.PI * 2
        );
        ctx.fill();

        ctx.beginPath();
        ctx.arc(
          charPos.x + size.width / 6,
          charPos.y - size.height / 2.5,
          size.width / 7,
          0,
          Math.PI * 2
        );
        ctx.fill();
        break;
      case "left":
        ctx.beginPath();
        ctx.arc(
          charPos.x - size.width / 6,
          charPos.y + size.height / 2.5,
          size.width / 7,
          0,
          Math.PI * 2
        );
        ctx.fill();

        ctx.beginPath();
        ctx.arc(
          charPos.x - size.width / 6,
          charPos.y - size.height / 2.5,
          size.width / 7,
          0,
          Math.PI * 2
        );
        ctx.fill();
        break;
      default:
    }

    ctx.fillStyle = "#f0d997";

    ctx.beginPath();
    ctx.arc(charPos.x, charPos.y, size.width / 2.5, 0, Math.PI * 2);
    ctx.fill();
  };

  for (let i = 0; i < 5; i++) {
    genMaze();
  }

  const player = { x: 0, y: 0, dir: "down", health: 10 };
  let playerPositioned = false;
  maze.forEach((row, i) => {
    row.forEach((col, j) => {
      if (
        (!col.left || !col.right || !col.top || !col.down) &&
        !playerPositioned
      ) {
        player.x = j;
        player.y = i;
        playerPositioned = true;
      }
    });
  });

  // const zombies = [];
  // for (let i = 0; i < 10; i++) {
  //   const zombie = {};
  //   maze.forEach((row, i) => {
  //     row.forEach((col, i) => {
  //
  //     });
  //
  //   });
  //
  // }

  const handleResize = () => {
    canvas.width = window.innerHeight * 2;
    canvas.height = window.innerHeight * 2;

    draw();
  };

  handleResize();

  window.addEventListener("resize", handleResize);

  draw();

  document.addEventListener("keydown", (e) => {
    switch (e.key.toLowerCase()) {
      case "w":
        if (!maze[player.y][player.x].top) {
          player.y -= 1;
        }
        player.dir = "up";
        break;
      case "a":
        if (!maze[player.y][player.x].left) {
          player.x -= 1;
        }
        player.dir = "left";
        break;
      case "s":
        if (!maze[player.y][player.x].down) {
          player.y += 1;
        }
        player.dir = "down";
        break;
      case "d":
        if (!maze[player.y][player.x].right) {
          player.x += 1;
        }
        player.dir = "right";
        break;
      case " ":
        switch (player.dir) {
          case "right":
            maze[player.y][player.x].right = false;
            if (player.x + 1 < maze[0].length) {
              maze[player.y][player.x + 1].left = false;
            }
            break;
          case "left":
            maze[player.y][player.x].left = false;
            if (player.x - 1 > -1) {
              maze[player.y][player.x - 1].right = false;
            }
            break;
          case "down":
            maze[player.y][player.x].down = false;
            if (player.y + 1 < maze.length) {
              maze[player.y + 1][player.x].top = false;
            }
            break;
          case "up":
            maze[player.y][player.x].top = false;
            if (player.y - 1 > -1) {
              maze[player.y - 1][player.x].down = false;
            }
            break;
          default:
        }
        break;
      case " ":
        switch (player.dir) {
          case "right":
            maze[player.y][player.x].right = false;
            if (player.x + 1 < maze[0].length) {
              maze[player.y][player.x + 1].left = false;
            }
            break;
          case "left":
            maze[player.y][player.x].left = false;
            if (player.x - 1 > -1) {
              maze[player.y][player.x - 1].right = false;
            }
            break;
          case "down":
            maze[player.y][player.x].down = false;
            if (player.y + 1 < maze.length) {
              maze[player.y + 1][player.x].top = false;
            }
            break;
          case "up":
            maze[player.y][player.x].top = false;
            if (player.y - 1 > -1) {
              maze[player.y - 1][player.x].down = false;
            }
            break;
          default:
        }
        break;

      case "l":
        maze[player.y][player.x].right = true;
        if (player.x + 1 < maze[0].length) {
          maze[player.y][player.x + 1].left = true;
        }
        break;
      case "j":
        maze[player.y][player.x].left = true;
        if (player.x - 1 > -1) {
          maze[player.y][player.x - 1].right = true;
        }
        break;
      case "k":
        maze[player.y][player.x].down = true;
        if (player.y + 1 < maze.length) {
          maze[player.y + 1][player.x].top = true;
        }
        break;
      case "i":
        maze[player.y][player.x].top = true;
        if (player.y - 1 > -1) {
          maze[player.y - 1][player.x].down = true;
        }
        break;
      default:
    }

    if (player.x < 0) {
      player.x = 0;
    } else if (player.x > maze[0].length - 1) {
      player.x = maze[0].length - 1;
    }
    if (player.y < 0) {
      player.y = 0;
    } else if (player.y > maze.length - 1) {
      player.y = maze.length - 1;
    }

    // updateAnims();

    draw();
  });

  // const ambiance = new Audio("audio/ambiance.mp3");
  // ambiance.loop = true;
  // ambiance.play();
};
