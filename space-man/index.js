const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

const resizeCanvas = () => {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
};

resizeCanvas();
window.addEventListener("resize", resizeCanvas);

class Ship {
  constructor(x, y) {
    this.x = x;
    this.y = y;

    this.dir = 0;
    this.vx = 0;
    this.vy = 0;
    this.health = 100;
    this.fuel = 100;
    this.mining = false;
    this.storage = {
      ice: 0,
      iron: 0,
      gold: 0,
    };
  }

  draw() {
    if (this.dir > Math.PI * 2) {
      this.dir -= Math.PI * 2;
    } else if (this.dir < 0) {
      this.dir += Math.PI * 2;
    }

    this.x += this.vx;
    this.y += this.vy;

    const dx = this.x - homePlanet.x;
    const dy = this.y - homePlanet.y;
    const length = Math.hypot(dx, dy);

    if (this.fuel < 0) {
      this.fuel = 0;
    }

    if (this.mining) {
      asteroids.forEach((asteroid, i) => {
        if (
          Math.hypot(this.x - asteroid.x, this.y - asteroid.y) < asteroid.size
        ) {
          this.storage[asteroid.type] += 0.05;
          asteroid.size -= 0.5;
          if (asteroid.size < 0) {
            asteroid.size = 0;
          }
        }

        if (this.storage[asteroid.type] > 50) {
          this.storage[asteroid.type] = 50;
        }
      });
    }

    if (this.health > 0) {
      if (length === homePlanet.size) {
        this.fuel = 100;
        this.storage = {
          ice: 0,
          iron: 0,
          gold: 0,
        };
      }

      ctx.fillStyle = "white";
    } else {
      ctx.fillStyle = "red";
    }

    ctx.save();

    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.rotate(this.dir);

    ctx.beginPath();
    ctx.moveTo(-10, 15);
    ctx.lineTo(10, 15);
    ctx.lineTo(0, -15);
    ctx.fill();

    ctx.restore();
  }
}

const playerShip = new Ship(0, 0);

class Asteroid {
  constructor(x, y, type) {
    this.x = x;
    this.y = y;
    this.type = type;

    this.vx = Math.random() * 2 - 1;
    this.vy = Math.random() * 2 - 1;

    this.size = Math.random() * 50 + 20;
  }

  draw() {
    this.x += this.vx;
    this.y += this.vy;

    let pos = {
      x: this.x - (playerShip.x - canvas.width / 2),
      y: this.y - (playerShip.y - canvas.height / 2),
    };

    const maxSize = 70;
    let hitEdge = false;

    if (pos.x + this.size < 0) {
      this.x = playerShip.x + (canvas.width / 2 + maxSize);
      this.y =
        playerShip.y + (Math.random() * canvas.height - canvas.height / 2);

      hitEdge = true;
    } else if (pos.x - this.size > canvas.width) {
      this.x = playerShip.x - (canvas.width / 2 + maxSize);
      this.y =
        playerShip.y + (Math.random() * canvas.height - canvas.height / 2);

      hitEdge = true;
    }

    if (pos.y + this.size < 0) {
      this.y = playerShip.y + (canvas.height / 2 + maxSize);
      this.x = playerShip.x + (Math.random() * canvas.width - canvas.width / 2);

      hitEdge = true;
    } else if (pos.y - this.size > canvas.height) {
      this.y = playerShip.y - (canvas.height / 2 + maxSize);
      this.x = playerShip.x + (Math.random() * canvas.width - canvas.width / 2);

      hitEdge = true;
    }

    pos = {
      x: this.x - (playerShip.x - canvas.width / 2),
      y: this.y - (playerShip.y - canvas.height / 2),
    };

    if (hitEdge) {
      this.vx = Math.random() * 2 - 1;
      this.vy = Math.random() * 2 - 1;

      const randNum = Math.floor(Math.random() * 3);
      if (randNum === 0) {
        this.type = "ice";
      } else if (randNum === 1) {
        this.type = "iron";
      } else {
        this.type = "gold";
      }

      this.size = Math.random() * 50 + 20;
    }

    switch (this.type) {
      case "ice":
        ctx.fillStyle = "lightblue";
        break;
      case "iron":
        ctx.fillStyle = "brown";
        break;
      case "gold":
        ctx.fillStyle = "gold";
        break;
      default:
    }

    ctx.beginPath();
    ctx.arc(pos.x, pos.y, this.size, 0, Math.PI * 2);
    ctx.fill();
  }
}

const asteroids = [];
for (let i = 0; i < 3; i++) {
  const randNum = Math.floor(Math.random() * 3);
  let type;
  if (randNum === 0) {
    type = "ice";
  } else if (randNum === 1) {
    type = "iron";
  } else {
    type = "gold";
  }

  const asteroid = new Asteroid(
    Math.random() * canvas.width - canvas.width / 2,
    Math.random() * canvas.height - canvas.height / 2,
    type
  );

  asteroids.push(asteroid);
}

class Planet {
  constructor(x, y, size, color) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.color = color;
  }

  draw() {
    let pos = {
      x: this.x - (playerShip.x - canvas.width / 2),
      y: this.y - (playerShip.y - canvas.height / 2),
    };

    const dx = playerShip.x - this.x;
    const dy = playerShip.y - this.y;
    const length = Math.hypot(dx, dy);

    if (length < this.size) {
      if (Math.abs(playerShip.vx) + Math.abs(playerShip.vy) > 1) {
        playerShip.health = 0;
      }

      playerShip.vx = 0;
      playerShip.vy = 0;
      playerShip.x = this.x + this.size * (dx / length);
      playerShip.y = this.y + this.size * (dy / length);
    }

    if (
      pos.x + this.size > 0 &&
      pos.x - this.size < canvas.width &&
      pos.y + this.size > 0 &&
      pos.y - this.size < canvas.height
    ) {
      ctx.fillStyle = this.color;

      ctx.beginPath();
      ctx.arc(pos.x, pos.y, this.size, 0, Math.PI * 2);
      ctx.fill();
    }
  }
}

const homePlanet = new Planet(0, 10000, 10000, "green");

const planets = [];
while (planets.length < 10) {
  const planet = new Planet(
    Math.random() * 100000 - 50000,
    Math.random() * 100000 - 50000,
    Math.random() * 20000 + 5000,
    `#${Math.floor(Math.random() * 16777215).toString(16)}`
  );

  if (
    Math.hypot(planet.x - homePlanet.x, planet.y - homePlanet.y) -
      (planet.size + homePlanet.size) >
    1000
  ) {
    planets.push(planet);
  }
}

const keys = {};

const checkKeys = () => {
  if (playerShip.fuel > 0) {
    if (keys.w) {
      playerShip.vx += Math.sin(playerShip.dir) * 0.03;
      playerShip.vy -= Math.cos(playerShip.dir) * 0.03;

      playerShip.fuel -= 0.01;
    }

    if (keys.a) {
      playerShip.dir -= 0.04;

      playerShip.fuel -= 0.005;
    }

    if (keys.d) {
      playerShip.dir += 0.04;

      playerShip.fuel -= 0.005;
    }
  }

  if (keys[" "]) {
    playerShip.mining = true;
  } else {
    playerShip.mining = false;
  }
};

const logStats = () => {
  ctx.fillStyle = "white";
  ctx.font = "24px sans";
  ctx.textAlign = "start";

  ctx.fillText(
    `Distance from home: ${
      Math.floor(
        Math.hypot(playerShip.x - homePlanet.x, playerShip.y - homePlanet.y)
      ) - homePlanet.size
    }m`,
    10,
    30
  );

  ctx.fillText(
    `Velocity: ${
      Math.floor((Math.abs(playerShip.vx) + Math.abs(playerShip.vy)) * 100) /
      100
    }`,
    10,
    60
  );

  ctx.fillText(`Health: ${playerShip.health}%`, 10, 90);

  if (playerShip.fuel < 10) {
    ctx.fillStyle = "red";
  }
  ctx.fillText(`Fuel: ${Math.floor(playerShip.fuel)}%`, 10, 120);

  ctx.fillStyle = "white";
  ctx.fillText(`Ice: ${Math.floor(playerShip.storage.ice)}`, 10, 150);
  ctx.fillText(`Iron: ${Math.floor(playerShip.storage.iron)}`, 10, 180);
  ctx.fillText(`Gold: ${Math.floor(playerShip.storage.gold)}`, 10, 210);

  // ctx.textAlign = "end";
  //
  // ctx.font = "40px sans";
  // ctx.fillText("Home Planet", canvas.width - 30, 50);
  //
  // ctx.font = "24px sans";
  // ctx.fillText("Economy: 0", canvas.width - 30, 100);
};

const startGame = () => {
  let menu = true;

  document.addEventListener("keydown", (e) => {
    keys[e.key] = true;

    if (playerShip.fuel > 0 && !menu) {
      if (e.key === "e") {
        playerShip.vx += Math.sin(playerShip.dir) * 20;
        playerShip.vy -= Math.cos(playerShip.dir) * 20;

        playerShip.fuel -= 10;
      }
      if (e.key === "q") {
        playerShip.fuel -=
          (Math.abs(playerShip.vx) + Math.abs(playerShip.vy)) / 2;

        playerShip.vx = 0;
        playerShip.vy = 0;
      }
    }

    if (e.key === "m") {
      menu = !menu;
    }
  });
  document.addEventListener("keyup", (e) => {
    delete keys[e.key];
  });

  const gameInterval = setInterval(() => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    if (!menu) {
      checkKeys();

      asteroids.forEach((asteroid, i) => {
        asteroid.draw();
      });

      ctx.strokeStyle = "green";
      ctx.lineWidth = 2;
      ctx.beginPath();
      ctx.moveTo(canvas.width / 2, canvas.height / 2);
      ctx.lineTo(
        homePlanet.x - (playerShip.x - canvas.width / 2),
        homePlanet.y - (playerShip.y - canvas.height / 2)
      );
      ctx.stroke();

      homePlanet.draw();

      ctx.strokeStyle = "white";
      planets.forEach((planet, i) => {
        const dx = planet.x - playerShip.x;
        const dy = planet.y - playerShip.y;
        const dist = Math.hypot(dx, dy) - planet.size;

        if (dist < 5000) {
          ctx.lineWidth = 2 - dist / 2500;

          ctx.beginPath();
          ctx.moveTo(canvas.width / 2, canvas.height / 2);
          ctx.lineTo(
            planet.x - (playerShip.x - canvas.width / 2),
            planet.y - (playerShip.y - canvas.height / 2)
          );
          ctx.stroke();
        }

        planet.draw();
      });

      playerShip.draw();

      ctx.strokeStyle = "blue";
      ctx.lineWidth = 3;
      ctx.beginPath();
      ctx.moveTo(canvas.width / 2, canvas.height / 2);
      ctx.lineTo(
        canvas.width / 2 + playerShip.vx * 10,
        canvas.height / 2 + playerShip.vy * 10
      );
      ctx.stroke();

      logStats();

      if (playerShip.health <= 0) {
        clearInterval(gameInterval);
      }
    } else {
      ctx.fillStyle = "white";
      ctx.textAlign = "center";

      ctx.font = "70px sans";
      ctx.fillText("Instructions", canvas.width / 2, 100);

      ctx.font = "24px sans";
      ctx.fillText("Press W to move foward", canvas.width / 2, 200);
      ctx.fillText("Press A/D to move left/right", canvas.width / 2, 230);
      ctx.fillText("To go into hyperdrive, press E", canvas.width / 2, 260);
      ctx.fillText("To cancel all movement, press Q", canvas.width / 2, 290);
      ctx.fillText(
        "While touching an asteroid press SPACE to mine it",
        canvas.width / 2,
        320
      );
      ctx.fillText("Press M to open/close this menu", canvas.width / 2, 350);
    }
  }, 1000 / 60);
};

startGame();
