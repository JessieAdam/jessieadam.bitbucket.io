const imagePrev = document.getElementById("image-prev");
const imageGetter = document.getElementById("image-getter");
const text1 = document.getElementById("text1");
const text1Edit = document.getElementById("text1-edit");
const text2 = document.getElementById("text2");
const text2Edit = document.getElementById("text2-edit");
const memeContainer = document.getElementById("meme-container");
const memeOutput = document.getElementById("meme-output");

text1.style.display = "none";
text2.style.display = "none";

const addImage = () => {
  if (imageGetter.files[0]) {
    text1.style.display = "block";
    text2.style.display = "block";

    imagePrev.innerHTML = `
      <img src="${URL.createObjectURL(
        imageGetter.files[0]
      )}" style="width: 50vw" />
    `;
  }
};

const editText = (textId) => {
  switch (textId) {
    case 1:
      text1.innerHTML = text1Edit.value;
      break;
    case 2:
      text2.innerHTML = text2Edit.value;
      break;
    default:
  }
};

text1Edit.addEventListener("input", () => editText(1));
text2Edit.addEventListener("input", () => editText(2));

const saveMeme = () => {
  html2canvas(memeContainer, { allowTaint: true }).then((canvas) => {
    memeOutput.innerHTML = "";
    memeOutput.appendChild(canvas);
  });
};
