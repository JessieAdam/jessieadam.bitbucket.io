const formElm = document.getElementById("password-form");
const outputElm = document.getElementById("password-output");

formElm.addEventListener("submit", (e) => {
  e.preventDefault();

  crackPassword(e.target.password.value);
});

let crackInterval;

const crackPassword = (password) => {
  // if (crackInterval) {
  //   clearInterval(crackInterval);
  // }

  outputElm.innerHTML = "";

  setTimeout(() => {
    const chars =
      "`1234567890-=qwertyuiop[]\\asdfghjkl;'zxcvbnm,./~!@#$%^&*()_+QWERTYUIOP{}|ASDFGHJKL:\"ZXCVBNM<>? ";
    let guess;
    // crackInterval = setInterval(() => {
    while (guess !== password) {
      guess = "";

      for (let i = 0; i < password.length; i++) {
        guess += chars[Math.floor(Math.random() * chars.length)];
      }
    }

    outputElm.innerHTML = guess;
  }, 10);
  // if (guess === password) {
  //   clearInterval(crackInterval);
  // }
  // }, 1);
};
